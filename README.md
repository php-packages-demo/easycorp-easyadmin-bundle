# [easycorp/easyadmin-bundle](https://packagist.org/packages/easycorp/easyadmin-bundle)

![Packagist Downloads](https://img.shields.io/packagist/dm/easycorp/easyadmin-bundle)

[Admin generator for Symfony applications](https://symfony.com/doc/current/bundles/EasyAdminBundle)

# Official documentation
* [*EasyAdmin Blog*](https://easycorp.github.io/blog/)
*  Symfony5: The Fast Track [*Step 9: Setting up an Admin Backend*
  ](https://symfony.com/doc/current/the-fast-track/en/9-backend.html)
* [*New in EasyAdmin 4: Menu Badges*
  ](https://easycorp.github.io/blog//posts/new-in-easyadmin-4-menu-badges)
  2022-01
  * [*New in EasyAdmin: Pretty URLs*
    ](https://dev.to/javiereguiluz/new-in-easyadmin-pretty-urls-2knk)
    2024-11 Javier Eguiluz
* [*EasyAdmin 3 is released*
  ](https://symfony.com/blog/easyadmin-3-is-released)
  2020-06 Symfony
* [*EasyAdmin 3 is released*
  ](https://easycorp.github.io/blog//posts/easyadmin-3-is-released)
  2020-06 Easycorp

# Unofficial documentation
* [*Translating entities in EasyAdmin with DoctrineBehaviors*
  ][2023-09-25:Translating]
  2023-09 Dmitry (DEV)
* [*Make Your Entities Sortable in EasyAdmin*
  ](https://jolicode.com/blog/make-your-entities-sortable-in-easyadmin)
  2023-06 Jorick Pepin (JoliCode)
* [*EasyAdmin 4 for admin panel based on PHP 8 and Symfony 6: Install and create a sample*
  ](https://dev.to/nabbisen/easyadmin-4-based-on-php-8-and-symfony-6-install-and-create-a-sample-4gcg)
  2022-03 Heddi Nabbisen
* [*Implementing a user workflow with Symfony and EasyAdmin3*
  ](https://www.strangebuzz.com/en/blog/implementing-a-user-workflow-with-symfony-and-easyadmin3)
  2021-06 COil
* [*Implementing CKEditor and CKFinder on EasyAdmin (Symfony5)*
  ](https://medium.com/suleyman-aydoslu/implementing-ckeditor-and-ckfinder-on-easyadmin-a269888771ce)
  2020-05 Süleyman Aydoslu
* [*Immutable dates with proper timezone with EasyAdmin and Symfony Forms*
  ](https://www.orbitale.io/2019/10/28/proper-dates-with-easyadmin.html)
  2019-10 
* [*EasyAdmin and Symfony 4: Image and file uploading without bundles*
  ](https://medium.com/@antho.cecc/easyadmin-and-symfony-4-image-and-file-uploading-without-bundles-3a3d44f53120)
  2019-07 Anthony Cecconato
* [*Symfony 4 upload images using EasyAdmin*
  ](https://medium.com/@hassanjuniedi/symfony-4-easyadmin-upload-images-f106a8b9e2b2)
  2019-04 Hassan Juniedi

[2023-09-25:Translating]:
  https://dev.to/dzhebrak/translating-entities-in-easyadmin-with-doctrinebehaviors-1odg
  (In this article we will look at how to translate entities in EasyAdmin. We will create an "Article" entity, where we can translate title, slug and content into several languages, as well as add the ability to filter by translatable fields.)

## In french
* [*Ajouter un champ de texte statique dans un formulaire EasyAdmin*
  ](https://jolicode.com/blog/ajouter-un-champ-de-texte-statique-dans-un-formulaire-easyadmin)
  2024-05 Xavier Lacot (JoliCode)
